﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DDDTechDogApi.Application.DTO.DTO;
using DDDTechDogApi.Application.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace DDDTechDogApi.Presentation.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AlunosController : ControllerBase
    {
        private readonly IApplicationServiceAluno _applicationServiceAluno;


        public AlunosController(IApplicationServiceAluno ApplicationServiceAluno)
        {
            _applicationServiceAluno = ApplicationServiceAluno;
        }
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return Ok(_applicationServiceAluno.GetAll());
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return Ok(_applicationServiceAluno.GetById(id));
        }

        // POST api/values
        [HttpPost]
        public ActionResult Post([FromBody] AlunoDTO alunoDTO)
        {
            try
            {
                if (alunoDTO == null)
                    return NotFound();

                _applicationServiceAluno.Add(alunoDTO);
                return Ok("Aluno Cadastrado com sucesso!");
            }
            catch (Exception ex)
            {

                throw ex;
            }


        }

        // PUT api/values/5
        [HttpPut]
        public ActionResult Put([FromBody] AlunoDTO alunoDTO)
        {
            try
            {
                if (alunoDTO == null)
                    return NotFound();

                _applicationServiceAluno.Update(alunoDTO);
                return Ok("Aluno Atualizado com sucesso!");
            }
            catch (Exception)
            {

                throw;
            }
        }

        // DELETE api/values/5
        [HttpDelete()]
        public ActionResult Delete([FromBody] AlunoDTO alunoDTO)
        {
            try
            {
                if (alunoDTO == null)
                    return NotFound();

                _applicationServiceAluno.Remove(alunoDTO);
                return Ok("Aluno Removido com sucesso!");
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
    }
}
