﻿using System;
using System.Collections.Generic;
using DDDTechDogApi.Application.DTO.DTO;
using DDDTechDogApi.Application.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace DDDTechDogApi.Presentation.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class EscolasController : ControllerBase
    {
        private readonly IApplicationServiceEscola _applicationServiceEscola;


        public EscolasController(IApplicationServiceEscola ApplicationServiceEscola)
        {
            _applicationServiceEscola = ApplicationServiceEscola;
        }
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return Ok(_applicationServiceEscola.GetAll());
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return Ok(_applicationServiceEscola.GetById(id));
        }

        // POST api/values
        [HttpPost]
        public ActionResult Post([FromBody] EscolaDTO escolaDTO)
        {
            try
            {
                if (escolaDTO == null)
                    return NotFound();

                _applicationServiceEscola.Add(escolaDTO);
                return Ok("Escola Cadastrado com sucesso!");
            }
            catch (Exception ex)
            {

                throw ex;
            }


        }

        // PUT api/values/5
        [HttpPut]
        public ActionResult Put([FromBody] EscolaDTO escolaDTO)
        {
            try
            {
                if (escolaDTO == null)
                    return NotFound();

                _applicationServiceEscola.Update(escolaDTO);
                return Ok("Escola Atualizado com sucesso!");
            }
            catch (Exception)
            {

                throw;
            }
        }

        // DELETE api/values/5
        [HttpDelete()]
        public ActionResult Delete([FromBody] EscolaDTO escolaDTO)
        {
            try
            {
                if (escolaDTO == null)
                    return NotFound();

                _applicationServiceEscola.Remove(escolaDTO);
                return Ok("Escola Removido com sucesso!");
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
    }
}
