﻿using System;
using System.Collections.Generic;
using DDDTechDogApi.Application.DTO.DTO;
using DDDTechDogApi.Application.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace DDDTechDogApi.Presentation.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class TurmasController : ControllerBase
    {

        private readonly IApplicationServiceTurma _applicationServiceTurma;


        public TurmasController(IApplicationServiceTurma ApplicationServiceTurma)
        {
            _applicationServiceTurma = ApplicationServiceTurma;
        }
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return Ok(_applicationServiceTurma.GetAll());
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return Ok(_applicationServiceTurma.GetById(id));
        }

        // POST api/values
        [HttpPost]
        public ActionResult Post([FromBody] TurmaDTO turmaDTO)
        {
            try
            {
                if (turmaDTO == null)
                    return NotFound();

                _applicationServiceTurma.Add(turmaDTO);
                return Ok("Turma Cadastrado com sucesso!");
            }
            catch (Exception ex)
            {

                throw ex;
            }


        }

        // PUT api/values/5
        [HttpPut]
        public ActionResult Put([FromBody] TurmaDTO turmaDTO)
        {
            try
            {
                if (turmaDTO == null)
                    return NotFound();

                _applicationServiceTurma.Update(turmaDTO);
                return Ok("Turma Atualizado com sucesso!");
            }
            catch (Exception)
            {

                throw;
            }
        }

        // DELETE api/values/5
        [HttpDelete()]
        public ActionResult Delete([FromBody] TurmaDTO turmaDTO)
        {
            try
            {
                if (turmaDTO == null)
                    return NotFound();

                _applicationServiceTurma.Remove(turmaDTO);
                return Ok("Turma Removido com sucesso!");
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

    }
}
