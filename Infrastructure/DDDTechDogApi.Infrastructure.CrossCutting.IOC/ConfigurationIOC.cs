﻿using Autofac;
using DDDTechDogApi.Application.Interfaces;
using DDDTechDogApi.Application.Services;
using DDDTechDogApi.Domain.Core.Interfaces.Repositorys;
using DDDTechDogApi.Domain.Core.Interfaces.Services;
using DDDTechDogApi.Domain.Services.Services;
using DDDTechDogApi.Infrastructure.CrossCutting.Adapter.Interfaces;
using DDDTechDogApi.Infrastructure.CrossCutting.Adapter.Map;
using DDDTechDogApi.Infrastructure.Repository.Repositorys;

namespace DDDTechDogApi.Infrastructure.CrossCutting.IOC
{
    public class ConfigurationIOC
    {
        public static void Load(ContainerBuilder builder)
        {
            #region Registra IOC

            #region IOC Application
            builder.RegisterType<ApplicationServiceAluno>().As<IApplicationServiceAluno>();
            builder.RegisterType<ApplicationServiceEscola>().As<IApplicationServiceEscola>();
            builder.RegisterType<ApplicationServiceTurma>().As<IApplicationServiceTurma>();
            #endregion

            #region IOC Services
            builder.RegisterType<ServiceAluno>().As<IServiceAluno>();
            builder.RegisterType<ServiceTurma>().As<IServiceTurma>();
            #endregion

            #region IOC Repositorys SQL
            builder.RegisterType<RepositoryAluno>().As<IRepositoryAluno>();
            builder.RegisterType<RepositoryEscola>().As<IRepositoryTurma>();
            builder.RegisterType<RepositoryTurma>().As<IRepositoryTurma>();
            #endregion

            #region IOC Mapper
            builder.RegisterType<MapperAluno>().As<IMapperAluno>();
            builder.RegisterType<MapperEscola>().As<IMapperEscola>();
            builder.RegisterType<MapperTurma>().As<IMapperTurma>();
            #endregion

            #endregion

        }
    }
}
