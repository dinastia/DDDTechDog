﻿using DDDTechDogApi.Domain.Core.Interfaces.Repositorys;
using DDDTechDogApi.Infrastructure.Data;
using DDDTechDogApi.Domain.Models;

namespace DDDTechDogApi.Infrastructure.Repository.Repositorys
{
    public class RepositoryEscola : RepositoryBase<Escola>, IRepositoryEscola
    {

        private readonly SqlContext _context;
        public RepositoryEscola(SqlContext Context)
            : base(Context)
        {
            _context = Context;
        }

    }
}
