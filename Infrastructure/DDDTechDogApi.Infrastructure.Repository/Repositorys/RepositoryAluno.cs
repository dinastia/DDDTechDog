﻿using DDDTechDogApi.Domain.Core.Interfaces.Repositorys;
using DDDTechDogApi.Infrastructure.Data;
using DDDTechDogApi.Domain.Models;

namespace DDDTechDogApi.Infrastructure.Repository.Repositorys
{
    public class RepositoryAluno : RepositoryBase<Aluno>, IRepositoryAluno
    {
        private readonly SqlContext _context;
        public RepositoryAluno(SqlContext Context)
            : base(Context)
        {
            _context = Context;
        }
    }
}
