﻿using DDDTechDogApi.Domain.Core.Interfaces.Repositorys;
using DDDTechDogApi.Infrastructure.Data;
using DDDTechDogApi.Domain.Models;

namespace DDDTechDogApi.Infrastructure.Repository.Repositorys
{
    public class RepositoryTurma : RepositoryBase<Turma>, IRepositoryTurma
    {

        private readonly SqlContext _context;
        public RepositoryTurma(SqlContext Context)
            : base(Context)
        {
            _context = Context;
        }

    }
}
