﻿using System;
using System.Linq;
using DDDTechDogApi.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace DDDTechDogApi.Infrastructure.Data
{
    public class SqlContext : DbContext
    {
        public SqlContext()
        {
        }
    
        public SqlContext(DbContextOptions<SqlContext> options) : base(options) { }

        public DbSet<Aluno> Alunos { get; set; }

        public DbSet<Escola> Escola { get; set; }

        public DbSet<Turma> Turma { get; set; }

    }
}
