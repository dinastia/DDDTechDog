﻿using System;
using DDDTechDogApi.Application.DTO.DTO;
using DDDTechDogApi.Domain.Models;
using System.Collections.Generic;

namespace DDDTechDogApi.Infrastructure.CrossCutting.Adapter.Interfaces
{
    public interface IMapperAluno
    {

        #region Mappers

        Aluno MapperToEntity(AlunoDTO AlunoDTO);

        IEnumerable<AlunoDTO> MapperListAlunos(IEnumerable<Aluno> Alunos);

        AlunoDTO MapperToDTO(Aluno Aluno);

        #endregion
    }
}
