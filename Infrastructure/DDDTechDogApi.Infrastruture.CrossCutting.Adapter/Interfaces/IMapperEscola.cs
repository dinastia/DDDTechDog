﻿using DDDTechDogApi.Application.DTO.DTO;
using DDDTechDogApi.Domain.Models;
using System.Collections.Generic;

namespace DDDTechDogApi.Infrastructure.CrossCutting.Adapter.Interfaces
{
    public interface IMapperEscola
    {

        #region Mappers

        Escola MapperToEntity(EscolaDTO EscolaDTO);

        IEnumerable<EscolaDTO> MapperListEscolas(IEnumerable<Escola> Escolas);

        EscolaDTO MapperToDTO(Escola Escola);

        #endregion
    }
}
