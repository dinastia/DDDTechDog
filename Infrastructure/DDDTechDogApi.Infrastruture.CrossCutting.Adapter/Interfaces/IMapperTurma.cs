﻿using DDDTechDogApi.Application.DTO.DTO;
using DDDTechDogApi.Domain.Models;
using System.Collections.Generic;

namespace DDDTechDogApi.Infrastructure.CrossCutting.Adapter.Interfaces
{
    public interface IMapperTurma
    {

        #region Mappers

        Turma MapperToEntity(TurmaDTO TurmaDTO);

        IEnumerable<TurmaDTO> MapperListTurmas(IEnumerable<Turma> Turmas);

        TurmaDTO MapperToDTO(Turma Turma);

        #endregion
    }
}
