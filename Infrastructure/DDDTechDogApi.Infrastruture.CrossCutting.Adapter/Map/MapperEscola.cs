﻿using DDDTechDogApi.Application.DTO.DTO;
using DDDTechDogApi.Domain.Models;
using DDDTechDogApi.Infrastructure.CrossCutting.Adapter.Interfaces;
using System.Collections.Generic;

namespace DDDTechDogApi.Infrastructure.CrossCutting.Adapter.Map
{
    public class MapperEscola : IMapperEscola
    {
        #region properties

        List<EscolaDTO> escolaDTOs = new List<EscolaDTO>();

        #endregion


        #region methods

        public Escola MapperToEntity(EscolaDTO escolaDTO)
        {
            Escola escola = new Escola
            {
                Id = escolaDTO.Id,
                Nome = escolaDTO.Nome,
                Logradouro = escolaDTO.Logradouro,
                Complemento = escolaDTO.Complemento,
                Bairro = escolaDTO.Bairro,
                Cidade = escolaDTO.Cidade,
                Estado = escolaDTO.Estado
            };

            return escola;

        }


        public IEnumerable<EscolaDTO> MapperListEscolas(IEnumerable<Escola> escolas)
        {
            foreach (var item in escolas)
            {


                EscolaDTO escolaDTO = new EscolaDTO
                {
                    Id = item.Id,
                    Nome = item.Nome,
                    Logradouro = item.Logradouro,
                    Complemento = item.Complemento,
                    Bairro = item.Bairro,
                    Cidade = item.Cidade,
                    Estado = item.Estado
                };



                escolaDTOs.Add(escolaDTO);

            }

            return escolaDTOs;
        }

        public EscolaDTO MapperToDTO(Escola Escola)
        {

            EscolaDTO escolaDTO = new EscolaDTO
            {
                Id = Escola.Id,
                Nome = Escola.Nome,
                Logradouro = Escola.Logradouro,
                Complemento = Escola.Complemento,
                Bairro = Escola.Bairro,
                Cidade = Escola.Cidade,
                Estado = Escola.Estado
            };

            return escolaDTO;

        }

        #endregion

    }

}
