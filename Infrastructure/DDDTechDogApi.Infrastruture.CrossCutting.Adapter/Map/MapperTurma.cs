﻿using DDDTechDogApi.Application.DTO.DTO;
using DDDTechDogApi.Domain.Models;
using DDDTechDogApi.Infrastructure.CrossCutting.Adapter.Interfaces;
using System.Collections.Generic;

namespace DDDTechDogApi.Infrastructure.CrossCutting.Adapter.Map
{
    public class MapperTurma : IMapperTurma
    {
        #region properties

        List<TurmaDTO> turmaDTOs = new List<TurmaDTO>();

        #endregion


        #region methods

        public Turma MapperToEntity(TurmaDTO turmaDTO)
        {
            Turma turma = new Turma
            {
                Id = turmaDTO.Id,
                Nome = turmaDTO.Nome,
                Capacidade = turmaDTO.Capacidade
            };

            return turma;

        }


        public IEnumerable<TurmaDTO> MapperListTurmas(IEnumerable<Turma> turmas)
        {
            foreach (var item in turmas)
            {


                TurmaDTO turmaDTO = new TurmaDTO
                {
                    Id = item.Id,
                    Nome = item.Nome,
                    Capacidade = item.Capacidade
                };



                turmaDTOs.Add(turmaDTO);

            }

            return turmaDTOs;
        }

        public TurmaDTO MapperToDTO(Turma Turma)
        {

            TurmaDTO turmaDTO = new TurmaDTO
            {
                Id = Turma.Id,
                Nome = Turma.Nome,
                Capacidade = Turma.Capacidade
            };

            return turmaDTO;

        }

        #endregion

    }

}
