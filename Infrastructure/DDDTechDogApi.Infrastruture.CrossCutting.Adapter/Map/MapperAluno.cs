﻿using DDDTechDogApi.Application.DTO.DTO;
using DDDTechDogApi.Domain.Models;
using DDDTechDogApi.Infrastructure.CrossCutting.Adapter.Interfaces;
using System.Collections.Generic;

namespace DDDTechDogApi.Infrastructure.CrossCutting.Adapter.Map
{

    public class MapperAluno : IMapperAluno
    {
            #region properties

            List<AlunoDTO> alunoDTOs = new List<AlunoDTO>();

            #endregion


            #region methods

            public Aluno MapperToEntity(AlunoDTO alunoDTO)
            {
                Aluno aluno = new Aluno
                {
                    Id = alunoDTO.Id,
                    Nome = alunoDTO.Nome,
                    DataDeNascimento = alunoDTO.DataDeNascimento
                };

                return aluno;

            }


            public IEnumerable<AlunoDTO> MapperListAlunos(IEnumerable<Aluno> alunos)
            {
                foreach (var item in alunos)
                {


                    AlunoDTO alunoDTO = new AlunoDTO
                    {
                        Id = item.Id,
                        Nome = item.Nome,
                        DataDeNascimento = item.DataDeNascimento
                    };



                    alunoDTOs.Add(alunoDTO);

                }

                return alunoDTOs;
            }

            public AlunoDTO MapperToDTO(Aluno Aluno)
            {

                AlunoDTO alunoDTO = new AlunoDTO
                {
                    Id = Aluno.Id,
                    Nome = Aluno.Nome,
                    DataDeNascimento = Aluno.DataDeNascimento
                };

                return alunoDTO;

            }

            #endregion

        }
    
}
