﻿using System;
namespace DDDTechDogApi.Application.DTO.DTO
{
    public class AlunoDTO
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public DateTime DataDeNascimento { get; set; }
    }
}
