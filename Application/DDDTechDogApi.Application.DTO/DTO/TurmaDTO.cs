﻿namespace DDDTechDogApi.Application.DTO.DTO
{
    public class TurmaDTO
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public int Capacidade { get; set; }
    }
}
