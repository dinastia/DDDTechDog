﻿using System.Collections.Generic;
using DDDTechDogApi.Application.DTO.DTO;
namespace DDDTechDogApi.Application.Interfaces
{
    public interface IApplicationServiceAluno
    {
        void Add(AlunoDTO obj);

        AlunoDTO GetById(int id);

        IEnumerable<AlunoDTO> GetAll();

        void Update(AlunoDTO obj);

        void Remove(AlunoDTO obj);

        void Dispose();
    }
}
