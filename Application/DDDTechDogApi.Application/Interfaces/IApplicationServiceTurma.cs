﻿using System.Collections.Generic;
using DDDTechDogApi.Application.DTO.DTO;
namespace DDDTechDogApi.Application.Interfaces
{
    public interface IApplicationServiceTurma
    {
        void Add(TurmaDTO obj);

        TurmaDTO GetById(int id);

        IEnumerable<TurmaDTO> GetAll();

        void Update(TurmaDTO obj);

        void Remove(TurmaDTO obj);

        void Dispose();
    }
}
