﻿using System.Collections.Generic;
using DDDTechDogApi.Application.DTO.DTO;
namespace DDDTechDogApi.Application.Interfaces
{
    public interface IApplicationServiceEscola
    {
        void Add(EscolaDTO obj);

        EscolaDTO GetById(int id);

        IEnumerable<EscolaDTO> GetAll();

        void Update(EscolaDTO obj);

        void Remove(EscolaDTO obj);

        void Dispose();
    }
}
