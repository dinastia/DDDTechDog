﻿using DDDTechDogApi.Application.DTO.DTO;
using DDDTechDogApi.Application.Interfaces;
using DDDTechDogApi.Domain.Core.Interfaces.Services;
using DDDTechDogApi.Infrastructure.CrossCutting.Adapter.Interfaces;
using System.Collections.Generic;

namespace DDDTechDogApi.Application.Services
{
    public class ApplicationServiceTurma : IApplicationServiceTurma
    {
        private readonly IServiceTurma _serviceTurma;
        private readonly IMapperTurma _mapperTurma;

        public ApplicationServiceTurma(IServiceTurma serviceTurma,
                                        IMapperTurma mapperTurma)
        {
            _serviceTurma = serviceTurma;
            _mapperTurma = mapperTurma;
        }

        public void Add(TurmaDTO obj)
        {
            var objTurma = _mapperTurma.MapperToEntity(obj);
            _serviceTurma.Add(objTurma);
        }

        public void Dispose()
        {
            _serviceTurma.Dispose();
        }

        public IEnumerable<TurmaDTO> GetAll()
        {
            var objProdutos = _serviceTurma.GetAll();
            return _mapperTurma.MapperListTurmas(objProdutos);
        }

        public TurmaDTO GetById(int id)
        {
            var objTurma = _serviceTurma.GetById(id);
            return _mapperTurma.MapperToDTO(objTurma);
        }

        public void Remove(TurmaDTO obj)
        {
            var objTurma = _mapperTurma.MapperToEntity(obj);
            _serviceTurma.Remove(objTurma);
        }

        public void Update(TurmaDTO obj)
        {
            var objTurma = _mapperTurma.MapperToEntity(obj);
            _serviceTurma.Update(objTurma);
        }
    }
}
