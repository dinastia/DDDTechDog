﻿using DDDTechDogApi.Application.DTO.DTO;
using DDDTechDogApi.Application.Interfaces;
using DDDTechDogApi.Domain.Core.Interfaces.Services;
using DDDTechDogApi.Infrastructure.CrossCutting.Adapter.Interfaces;
using System.Collections.Generic;

namespace DDDTechDogApi.Application.Services
{
    public class ApplicationServiceEscola : IApplicationServiceEscola
    {
        private readonly IServiceEscola _serviceEscola;
        private readonly IMapperEscola _mapperEscola;

        public ApplicationServiceEscola(IServiceEscola serviceEscola,
                                        IMapperEscola mapperEscola)
        {
            _serviceEscola = serviceEscola;
            _mapperEscola = mapperEscola;
        }

        public void Add(EscolaDTO obj)
        {
            var objEscola = _mapperEscola.MapperToEntity(obj);
            _serviceEscola.Add(objEscola);
        }

        public void Dispose()
        {
            _serviceEscola.Dispose();
        }

        public IEnumerable<EscolaDTO> GetAll()
        {
            var objProdutos = _serviceEscola.GetAll();
            return _mapperEscola.MapperListEscolas(objProdutos);
        }

        public EscolaDTO GetById(int id)
        {
            var objEscola = _serviceEscola.GetById(id);
            return _mapperEscola.MapperToDTO(objEscola);
        }

        public void Remove(EscolaDTO obj)
        {
            var objEscola = _mapperEscola.MapperToEntity(obj);
            _serviceEscola.Remove(objEscola);
        }

        public void Update(EscolaDTO obj)
        {
            var objEscola = _mapperEscola.MapperToEntity(obj);
            _serviceEscola.Update(objEscola);
        }
    }
}
