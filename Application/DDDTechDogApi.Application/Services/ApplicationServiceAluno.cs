﻿using DDDTechDogApi.Application.DTO.DTO;
using DDDTechDogApi.Application.Interfaces;
using DDDTechDogApi.Domain.Core.Interfaces.Services;
using DDDTechDogApi.Infrastructure.CrossCutting.Adapter.Interfaces;
using System.Collections.Generic;

namespace DDDTechDogApi.Application.Services
{
    public class ApplicationServiceAluno : IApplicationServiceAluno
    {
        private readonly IServiceAluno _serviceAluno;
        private readonly IMapperAluno _mapperAluno;

        public ApplicationServiceAluno(IServiceAluno serviceAluno,
                                        IMapperAluno mapperAluno)
        {
            _serviceAluno = serviceAluno;
            _mapperAluno = mapperAluno;
        }

        public void Add(AlunoDTO obj)
        {
            var objAluno = _mapperAluno.MapperToEntity(obj);
            _serviceAluno.Add(objAluno);
        }

        public void Dispose()
        {
            _serviceAluno.Dispose();
        }

        public IEnumerable<AlunoDTO> GetAll()
        {
            var objProdutos = _serviceAluno.GetAll();
            return _mapperAluno.MapperListAlunos(objProdutos);
        }

        public AlunoDTO GetById(int id)
        {
            var objAluno = _serviceAluno.GetById(id);
            return _mapperAluno.MapperToDTO(objAluno);
        }

        public void Remove(AlunoDTO obj)
        {
            var objAluno = _mapperAluno.MapperToEntity(obj);
            _serviceAluno.Remove(objAluno);
        }

        public void Update(AlunoDTO obj)
        {
            var objAluno = _mapperAluno.MapperToEntity(obj);
            _serviceAluno.Update(objAluno);
        }
    }
}
