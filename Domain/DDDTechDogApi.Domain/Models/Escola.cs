﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DDDTechDogApi.Domain.Models
{
    public class Escola : Endereco
    {
        public int Id { get; set; }
        public string Nome { get; set; }

    }
}
