﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DDDTechDogApi.Domain.Models
{
    public class Aluno : Base
    {
        public string Nome { get; set; }
        public DateTime DataDeNascimento { get; set; }

        public List<Turma> Turma { get; set; }
    }
}
