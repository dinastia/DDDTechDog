﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DDDTechDogApi.Domain.Models
{
    public class Turma : Base
    {
        public string Nome { get; set; }
        public int Capacidade { get; set; }

        [ForeignKey("Aluno")]
        public int AlunoId { get; set; }

        public virtual Aluno Aluno { get; set; }
        public List<Escola> Escola { get; set; }
    }
}
