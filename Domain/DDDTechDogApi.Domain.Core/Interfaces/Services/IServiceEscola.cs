﻿using DDDTechDogApi.Domain.Models;

namespace DDDTechDogApi.Domain.Core.Interfaces.Services
{
    public interface IServiceEscola : IServiceBase<Escola>
    {

    }
}
