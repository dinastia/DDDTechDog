﻿using DDDTechDogApi.Domain.Models;

namespace DDDTechDogApi.Domain.Core.Interfaces.Repositorys
{
    public interface IRepositoryAluno : IRepositoryBase<Aluno>
    {
    }
}
