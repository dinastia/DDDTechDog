﻿using DDDTechDogApi.Domain.Models;

namespace DDDTechDogApi.Domain.Core.Interfaces.Repositorys
{
    public interface IRepositoryTurma : IRepositoryBase<Turma>
    {
    }
}
