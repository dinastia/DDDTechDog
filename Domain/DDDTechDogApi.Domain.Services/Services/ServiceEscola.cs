﻿using DDDTechDogApi.Domain.Core.Interfaces.Repositorys;
using DDDTechDogApi.Domain.Core.Interfaces.Services;
using DDDTechDogApi.Domain.Models;

namespace DDDTechDogApi.Domain.Services.Services
{
    public class ServiceEscola : ServiceBase<Escola>, IServiceEscola
    {
        public readonly IRepositoryEscola _repositoryEscola;

        public ServiceEscola(IRepositoryEscola RepositoryEscola)
            : base(RepositoryEscola)
        {
            _repositoryEscola = RepositoryEscola;
        }

    }
}
