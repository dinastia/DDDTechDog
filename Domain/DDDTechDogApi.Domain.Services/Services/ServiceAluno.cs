﻿using DDDTechDogApi.Domain.Core.Interfaces.Repositorys;
using DDDTechDogApi.Domain.Core.Interfaces.Services;
using DDDTechDogApi.Domain.Models;

namespace DDDTechDogApi.Domain.Services.Services
{
    public class ServiceAluno : ServiceBase<Aluno>, IServiceAluno
    {
        public readonly IRepositoryAluno _repositoryAluno;

        public ServiceAluno(IRepositoryAluno RepositoryAluno)
            : base(RepositoryAluno)
        {
            _repositoryAluno = RepositoryAluno;
        }

    }
}
