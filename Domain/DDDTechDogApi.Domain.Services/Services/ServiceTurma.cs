﻿using DDDTechDogApi.Domain.Core.Interfaces.Repositorys;
using DDDTechDogApi.Domain.Core.Interfaces.Services;
using DDDTechDogApi.Domain.Models;

namespace DDDTechDogApi.Domain.Services.Services
{
    public class ServiceTurma : ServiceBase<Turma>, IServiceTurma
    {
        public readonly IRepositoryTurma _repositoryTurma;

        public ServiceTurma(IRepositoryTurma RepositoryTurma)
            : base(RepositoryTurma)
        {
            _repositoryTurma = RepositoryTurma;
        }

    }
}
